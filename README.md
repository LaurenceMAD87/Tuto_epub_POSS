# POSS 2016

## Liste des étapes pour aller du fichier .md jusqu’au .epub framasoftisé et accessible.



* regarder le fichier .md et le .pdf pour comprendre la structure du livre. Dans ce cas présent on a juste le .md. (le fichier PDF est accessible sur le cloud Framabook par ce [lien](http://owncloud.framabook.org/index.php/s/e2ZEApeWtGMR1mP).
* reprendre d’un epub existant la structure des dossiers et fichiers de l’epub.  
-- ajout du fichier mimetype. Il ne change jamais. Ne pas le modifier. Il est à ajouter à la racine du projet.  
-- ajout du dossier META-INF et du fichier container.xml. Le fichier container.xml donne le nom du dossier où seront rangés les fichiers de contenu. Ce sont des fichiers aux formats OPS soit OEBPS. En général j’utilise OPS.  
  
  

### Repérage de la structure du livre 

![structureLivre](https://framagit.org/goofy/POSS_2106/raw/914fe9e09497961e94536740775b0b8107ca8f04/howto/images/structureLivre.png)

### Chapitre avant-propos : Avant-propos (Nina Cercy)

La donnée, enjeu majeur du numérique

Données personnelles : histoire d'une dépossession

Souveraineté et autonomie numériques

#### Chapitre intro : Logiciel libre et autonomie numérique (Tristan Nitot)

### Chapitre 1 : Données personnelles et nouveaux usages

Reprendre le contrôle sur ses données, oui, mais pour quoi faire ?

Qu'est-ce qui est fait aujourd'hui de mes données personnelles ?

Mais alors, pourquoi se les ré-approprier ?

Et concrètement ?

**Interview : À la recherche de l'autonomie perdue (Daniel Kaplan)**

### Chapitre 2 Sensibiliser : les difficultés de porter un discours sur la souveraineté

Sensibiliser les internautes

Faire changer les pratiques quotidiennes

L'éducation numérique, un enjeu primordial

**Interview : Sensibiliser est un sport de combat (Adrienne Charmet)**

### Chapitre 3 : L'accessibilité, pour un numérique inclusif

L'accessibilité, en quoi ça consiste ?

Un souci d'égalité dans un monde de plus en plus numérique

**Interview : Du privilège À la liberté (Armony Altinier)**

### Chapitre 4 : La souveraineté numérique et l'état

Souveraineté et état de droit

De nombreux discours étatiques sur le numérique

**Interview : Remettre les géants au pas (Isabelle Falque-Pierrotin)**

**Interview : Réguler pour mieux régner (Charles Schulz)**

### Chapitre 5 : Souveraineté numérique et modèles d'affaires

Le piège de la gratuité

Comment en sortir ?

**Interview : Déconstruire la gratuité (Fabrice Rochelandet)**

### Chapitre 6 : Proposer des alternatives crédibles

L'autonomie, une histoire de libre choix

Les alternatives existantes

Le design, prochain défi du logiciel libre

**Interview : Framasoft, de l'esprit du Libre (Pierre-Yves Gosset)**

### Chapitre 7 : Blockchain et droit À l'oubli

**Article : La blockchain : comment réguler sans autorité (Primavera De Filippi, Michel Reymond)**

### Chapitre 8 : Construire un discours sur l'autonomie

Comment défendre l'autonomie numérique ?

Porter une vision alternative

**Interview : Du software au soft power (Christophe Masutti)**

**Interview : Pour un combat des imaginaires (Alain Damasio)**

### Chapitre : Conclusion  

Renverser l'oligopole  

Décentraliser  

Faire évoluer nos pratiques numériques

## à faire :

* Choisir comment on va découper le livre en plusieurs fichiers
* regarder comment est écrit le livre pour savoir quelles classes particulières seront nécessaires
* copier les 2 fichiers .css communs aux framabooks et créer celui particulier à cet ouvrage
*   transformer le fichier du langage de départ en HTML
*   créer le fichier chapitre1.xhtml qui servira de base aux autres fichiers. Ici on a un fichier source unique pour l’ensemble du livre donc copier-coller la partie du texte du fichier source dans le fichier chapitre1.xhtml
* changer les id des chapitres pour qu’ils soient plus simples. Comme la table des matières sera faite à la main, c’est plus facile d’avoir des id du type « ch1-1 » que « reprendre-le-contrôle-sur-ses-données-oui-mais-pour-quoi-faire »
* /!\il faut bien sûr que le niveau de titre le plus élevé soit le h1. Si le livre à des parties alors le h1 désignera les titres des parties, si on n’a que des chapitres alors le h1 sera pour les titres de chapitre.
* Idem faire attention à la ligne « body epub:type="bodymatter chapter" », https://idpf.github.io/epub-vocabs/structure/ permet de définir ce qu’il y aura dans le body/section, ça fait partie de la sémantique et ça différencie un bel epub d’un autre.
* Bien faire attention aux niveaux de titres. Dans le livre sur le POSS. Il y a des sortes de parties avec 2 à trois petits chapitres dedans. Le premier fichier est donc très important. 
* Dans ce cas présent j’ai du mal à bien voir comment le livre s’articule et donc de savoir comment faire la sémantique. J’ai choisi de faire un fichier par partie avec dans « body epub:type="bodymatter part" »
 puis section epub:type="bodymatter chapter".
  changer les ' par des ’ (avec vim « :%s/'/’/g »
* Vérifier que les em soient bien des vrais em et pas des changements de langue i lang="en" xml:lang="en". Pour cela je fais « /em » puis je navigue de em en em avec n, je supprime la balise quand nécessaire et ajoute avec une macro le i lang="en" xml:lang="en" /i
* a11y : ajout des changements de langues qui peuvent être utiles. Quand le mot est étranger mais est une marque on utilise la classe i lang="en" xml:lang="en" class="droit"droit/i. Pour MAIF si on ne fait rien ça sera lu « maif » et si on met juste une balise abbr ça sera lu M A I F, ce qui ne sera pas plus compréhensible, du coup j’ai mis une abbr avec un title="ma if"
* Regarder la table des matière, la structure du livre permet de savoir comment on va organiser le travail.
* Chaque livre est différent. Pour celui-ci il faut aller assez vite, on a une date butoir. Je commence donc à travailler avant que le fichier .pdf existe. Il faut donc voir avec le responsable de l’édition papier (Christophe) si on a bien compris la structure du livre.
Dans le cas présent, c’est des parties et des chapitres (8 parties) ou des chapitres et des sous-chapitres.
* On peut voir aussi que le livre va avoir beaucoup de questions-réponses. Dans le fichier d’origine il y a une classe spéciale pour les questions. Il va falloir la rajouter au .css. Comme c’est une classe propre à ce livre, elle sera définie dans le fichier poss.css.
* On a fait 3 fichiers .css. Le premier framabook.css est celui de l’ensemble des framabooks. Il sert aussi de maitre aux 2 autres. C’est celui qui est appelé dans les fichiers .xhtml.
* Le relecture.css ne sert que pendant la partie travail du framabook. Il permet de voir les balises sémantiques. Si on a bien mis les mots en langues étrangères avec la balise i, si les abréviations sont bien notées…
* le poss.css, change de nom à chaque epub et est le fichier propre à cet epub. Il contient assez peu de classes et est remis à zéro à chaque epub.
* Ensuite il faut transformer le .md en html. J’utilise pandoc avec la ligne de commande :  
  
 `pandoc -f markdown -t html -o ouvrageposs2016entier.html -s ouvrageposs2016entier.md`

* Création dans le poss.css des classes nécessaires à ce livre : auteur-intro, auteur, question. Comme je suis nulle en css, je m’en occuperai par la suite.
* Création du fichier chapitre1.xhtml qui servira de base pour tous les autres fichiers de chapitre. Avec un epub on choisi le découpage des fichiers. Il n’y a pas de règles pour le découpage il faut juste faire attention que les fichiers .xhtml ne soient pas trop lourds.  
-- J’ai eu un roman, tout simple sans images, qui n’avait qu’un fichier pour l’ensemble du texte. Il était très long pour afficher les pages. On pouvait tourner la page sur la liseuse, la lire avant qu’elle ne passe vraiment à la page suivante.  

Comme ici le livre n’est pas très gros (environ 138 pages en PDF) et peu d’images, on peut faire 8 chapitres plus les autres fichiers.  
-- Sur un livre de droit par exemple, j’ai découpé en sous-chapitres.


